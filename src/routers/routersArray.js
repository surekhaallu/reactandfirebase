export const routes = [
    {
        path:"/login",
        component:'Login',
        layout:'LoginAndRegistration'
    },
    {
        path:'/registration',
        component: 'Registration',
        layout:'LoginAndRegistration'
    },
];