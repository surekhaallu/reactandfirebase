import React from "react";
import {Switch} from 'react-router-dom'
import {AppRouter} from '../hoc/AppRouter'
import {routes as Routers} from './routersArray'


const Ro = () => {
    console.log()
    return(
        <div>
            <Switch>
                {Routers.map((el) => <AppRouter path={el.path} layout={el.layout} component={el.component}/>)}
            </Switch>
        </div>
    )
};

export default Ro;